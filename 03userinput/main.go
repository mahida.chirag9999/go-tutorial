package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	welcome := "Welcome to user input"
	fmt.Println(welcome)

	reader :=bufio.NewReader(os.Stdin)
	fmt.Println("enter the rating to our pizza:")

	// comma ok || err err
	input,_:=reader.ReadString('\n')
	// input,err:=reader.ReadString('\n')
	fmt.Println("Thanks for reading,",input)
 
}