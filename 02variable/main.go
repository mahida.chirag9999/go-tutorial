package main

import (
	"fmt"
)

const LoginToken string = "ghghghgh" //public

func main() {
	var username string = "chirag"
	fmt.Println(username)
	fmt.Printf("variable is of type: %T \n",username)

	var isLoggedIn bool = false
	fmt.Println(isLoggedIn)
	fmt.Printf("variable is of type: %T \n",isLoggedIn)

	var smallval uint8 = 255
	fmt.Println(smallval)
	fmt.Printf("variable is of type: %T \n",smallval)

	var smallfloat float64 = 255.4555739817391
	fmt.Println(smallfloat)
	fmt.Printf("variable is of type: %T \n",smallfloat)

	//defult values and some aliases
	var anothervariable int
	fmt.Println(anothervariable)
	fmt.Printf("variable is of type: %T \n",anothervariable)

	//another way to declare variable
	//implicit type
	var website ="chiragmahida.me"
	fmt.Println(website)

	//no var style [not allowed in outside of function like global]
	numberofUser := 300000
	fmt.Println(numberofUser)

	fmt.Println(LoginToken)
	fmt.Printf("variable is of type: %T \n",LoginToken)
}